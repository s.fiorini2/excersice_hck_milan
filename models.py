
# Create your models here.
#Django
from django.db                      import models
from django.utils                   import timezone
from apps.estado_paros_deta2.models import paros_deta2
#Modelos

class paros_deta3(models.Model):

    fecha_creacion      =  models.DateTimeField(default=timezone.now)
    fecha_modificacion  =  models.DateTimeField(auto_now=True)
    nombre              =  models.CharField(max_length=250, blank=False, null=False, unique=True)
    paros_deta2         =  models.ForeignKey(paros_deta2, on_delete=models.CASCADE, blank=True, null=True)
    ID_paros_deta3      =  models.IntegerField(null=True)
    prioridad           =  models.IntegerField(null=True)
    indicacion          =  models.CharField(max_length=150, blank=True, null=True, unique=False)

    def __str__(self):
        return self.nombre
