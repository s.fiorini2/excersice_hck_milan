# SMART POLLUTION DETECTION
# Version V1.0


Description. This project shows the detection of contaminated areas, as well as the integration of IoT devices to calculate the consumption of air contaminated by users. And the estimation of optimal number of vehicles by city regions to obtain admissible levels of pollution.

![arquitecture_d1_300px](documentation/arquitecture_d1.png)


Version Table:


| Functions                       | v1.0  |
| :---                            | :---: |
| Proyect in gitlab               | OK    |
| Data source pollution   (1)     | OK    |  
| Vehicle traffic data source (1) | D     |
| Wind speed data source  (1)     | D     |
| Brainstorm                      | D     |
| Backend Design (DJANGO) (3)     | D     |  


OK: Functionality completed
X : Functionality removed.
D : Functionality in development.



Backend Development using DJANGO [https://www.djangoproject.com/]


## Data sources
### Pullotion Data sources
#### Web page[https://docs.openaq.org]
#### Get Measurements using API-REST server [https://api.openaq.org/v1/measurements]
Data format JSON:
```
{"location":"Coyhaique",
 "parameter":"pm25",
 "date":{"utc":"2028-08-05T20:00:00.000Z",
         "local":"2028-08-05T16:00:00-04:00"},
  "value":81,
  "unit":"µg/m³",
  "coordinates":{"latitude":-45.57996447948,
                "longitude":-72.061125987766},
  "country":"CL","city":"Coyhaique"}
```


### Weather Data source:
#### This is an Open Database search engine [https://rapidapi.com/search/weather]
